<html><head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Arduíno</title>




        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/administrador.css" rel="stylesheet">
    </head>
    <body>
        
        <div class="container-fluid">
            <?php
	    require('menu.php');
	   /* if($_SESSION['usuario']!='admin'){
		header('location: index.php');
	    }*/
	    /*if(!isset($_SESSION['usuario'])){
		header('location: index.php');
	    }*/
	    ?>
        </div>
        <div class="topo"></div>
        <div class="container-fluid" class="jumbotron">
                <div class="row">
                    <div class="topo"></div>
                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                        <div class="btn-group" role="group">
                            <a href="cadastroNoticia.php" class="btn btn-default btn-responder">Cadastro de Notícias</a>
                        </div>                       
                        <div class="btn-group" role="group">
                            <a href="listagemUsuarios.php" class="btn btn-default">Lista de Usuários</a>
                        </div>
                        <div class="btn-group" role="group">
                            <a href="perguntasUsuarios.php" class="btn btn-default">Dúvidas dos Usuários</a>
                        </div>
                    </div>
                </div>
        </div>
                <div class="row">
                    <div class='col-md-2'></div>
                        <div class="col-md-8">
                            <h3 class="text-center"><b>Cadastrar notícia</b></h3>
                            <form role="form" action="processarNoticia.php" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="exampleInputText">Título da notícia</label>
                                    <input type="text" class="form-control" id="exampleInputText" name="Noticia" required/>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputText">Texto da notícia</label>
                                    <textarea class="form-control" id="exampleInputText" rows="15" name="textoNoticia" required/></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputText">Link da notícia</label>
                                    <input type="text" class="form-control" id="exampleInputLink" name="Link" required/>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Imagem</label>
                                    <input type="file" name="arquivoImg">
                                    <p class="help-block"></p>
                                </div>
                                <button type="submit" class="btn btn-adm">
                                    <b>Cadastrar</b>
                                </button>
                            </form>
                        </div>
                    <div class='col-md-2'></div>
                </div>

        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/scripts.js"></script>
        <script src="js/MascaraValidacao.js"></script>

        
        <?php 
        require 'footer.php' ?>

</body></html>