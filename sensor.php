<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Arduíno</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/sensor.css" rel="stylesheet">
        <script>
            function buscaCidades(str) {
                var xmlhttp = new XMLHttpRequest();

                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                        document.getElementById("divCidade").innerHTML = xmlhttp.responseText;
                }
                xmlhttp.open("GET", "listaCidades.php?uf=" + str, true);
                xmlhttp.send();
            }
        </script>
    </head>
    <body>
        <?php
        require('menu.php');
        ?>   
        <div class="container-fluid">
            <div class="row">
                <div class="topo"></div>
                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <a href="sensores.php" class="btn btn-default ">Sensores</a>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="parametros.php" class="btn btn-default">Parâmetros</a>
                    </div>
                </div>
            </div>



            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="jumbotron"> 
                            <form role="form" action="processarMonitoramento.php" method="POST">
                                <h1 class="text-center">Criar monitoramento</h1> 
                                <div class="form-group has-feedback">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="control-label" for="exampleInputEmail1">Nome</label>
                                            <input type="text" class="form-control" id="" name="Nome" required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <b>Descrição</b>
                                            <textarea name="Descricao" class="form-control" id="exampleInputDuvida" rows='3' required/></textarea>

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-1"></div> 
                                    <div class="col-md-3">
                                        <label for="exampleInputEstado">Estado</label>
                                        <br>
                                        <div class="option">
                                            <?php
                                            require('listaUFs.php');
                                            ?>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div>
                                            <label for="exampleInputCidade">Cidade</label>
                                            <br>
                                            <div  id="divCidade" class="option">
                                                <?php
                                                require('listaCidades.php');
                                                ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div>
                                            <label for="exampleInputCidade">Local</label>
                                            <br>
                                            <div  id="divLocal" class="option">
                                                <?php
                                                require('listalocalidade.php');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">  
                                    <div class="col-md-1"></div>
                                    <div class="col-md-3">
                                        <div>
                                            <label for="exampleInputCidade">Tipo de Solo</label>
                                            <br>
                                            <div  id="divSolo" class="option">
                                                <?php
                                                require('listatiposolo.php');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                <button type="submit" class="btn btn-criar" ><b>
                                        Criar
                                        <span class="glyphicon glyphicon-saved" aria-hidden="true"></span></b>
                                </button>
                            </form>  
                        </div>
                    </div>
                </div>
            </div>
        </div>





        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/scripts.js"></script></div></div>
<div class="section">                          



</body>
</html>
