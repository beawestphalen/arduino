<html>
<head>
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Arduíno</title>
        <meta name="description" content="Source code generated using layoutit.com">
        <meta name="author" content="LayoutIt!">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/problemas.css" rel="stylesheet">
   


        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/scripts.js"></script>
    
</head>

<body>
<?php 
	require('conexao.php');
    require('menu.php');
?>
<div class="container-fluid">
    <div class="row">
        <div class="form-group">
            <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="margintop">
                        <div class="jumbotron">
                            <div class="row">
                                <div class="col-md-12">
                                    <form action='processarProblemas.php' method='POST'>
                                        <?php
                                            $email = $_POST['emailRecuperar'];

                                            $resultado = mysqli_query($con, "SELECT pergunta_seguranca FROM usuario WHERE email= '$email'");

                                                while($consulta = mysqli_fetch_array($resultado)) {
                                                     echo("<div class='row'><div class='col-md-12'><h4>Pergunta de segurança: ".$consulta['pergunta_seguranca']."</h4></div></div>");
                                                }

                                                 echo "<input type='hidden' value=".$email." name='email'/>";
                                        ?>
                                        <br>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="exampleInputResposta"><h4>Resposta de segurança: </h4></label>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" id="" name="resposta" required/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-6">
                                                    <button type="submit" class="btn btn-noticia" ><b>
                                                        Enviar
                                                        <span class="glyphicon glyphicon-check" aria-hidden="true"></span></b>
                                                    </button>
                                            </div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>


                <div class="modal fade" id="modal-container-905037" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                               
                                <h4 class="modal-title" id="myModalLabel" style="color:black;">Problemas?</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form" method="POST" action="problemas.php">
                                    <div class="form-group">
                                        <label for="exampleInputEmail" style="color:black;">Insira seu e-mail para redefinir a senha:</label>
                                        <input type="email" class="form-control" id="exampleInputEmail" name="emailRecuperar">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-noticia">
                                            <b>Enviar
                                            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                                            </b>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
           
</body>
</html>


