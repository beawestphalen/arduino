<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Arduíno</title>

        <meta name="description" content="Source code generated using layoutit.com">
        <meta name="author" content="LayoutIt!">

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/cadastro.css" rel="stylesheet">
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/scripts.js"></script>
        <script src="js/MascaraValidacao.js"></script>
        <script>
            function buscaCidades(str) {
                var xmlhttp = new XMLHttpRequest();

                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                        document.getElementById("divCidade").innerHTML = xmlhttp.responseText;
                }
                xmlhttp.open("GET", "listaCidades.php?uf=" + str, true);
                xmlhttp.send();
            }

            function mascara(t, mask) {
                var i = t.value.length
                var saida = mask.substring(1, 0);
                var texto = mask.substring(i)
                if (texto.substring(0, 1) != saida) {
                    t.value += texto.substring(0, 1);
                }
            }
        </script>
    </head>
    <body>

        <div class="container-fluid">
            <?php require('menu.php'); ?>
            <div class="form">
                <div class="row">
                    <form role="form" action="processarCadastro.php" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="jumbotron">
                                    <div class="row">
                                        <h1 class="cadastro">CADASTRE-SE </h1>
                                        <div class="row">
                                            <div class="col-md-1"></div>   
                                            <div class="col-md-4">	 
                                                <label for="exampleInputNome">Nome completo</label>
                                                <input type="text" class="form-control" id="" name="Nome" required/>
                                            </div>				
                                            <div class="col-md-3">
                                                <label for="exampleInputData">Data de nascimento</label>
                                                <input type="date" class="form-control" id="" name="Data" required/>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Sexo</label>
                                                <div class="radio">
                                                    <label><input type="radio" name="Sexo" value="Feminino" required/>Feminino</label>
                                                    <label><input type="radio" name="Sexo" value="Masculino" required/>Masculino</label>
                                                </div>       
                                            </div>  
                                        </div>
                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-4">
                                                <label for="exampleInputUsuario">Usuário</label>
                                                <input type="text" class="form-control" id="" name="Usuario" required/>
                                            </div>

                                            <div class="col-md-3"> 
                                                <label for="exampleInputPassword1">Senha</label>
                                                <input type="password" class="form-control" id="" name="Password1" required/>
                                            </div>

                                            <div class="col-md-3">
                                                <label for="exampleInputPassword2">Confirmar senha</label>
                                                <input type="password" class="form-control" id="" name="Password2" required/>
                                            </div>
                                            <div class="col-md-1"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-1"></div> 
                                            <div class="col-md-5">
                                                <label for="exampleInputEmail1">E-mail</label>
                                                <input type="email" class="form-control" id="" name="Email" required/>
                                            </div>
                                            <div class="col-md-5">
                                                <label for="exampleInputTelefone">Celular</label>
                                                <input type="text" class="form-control" onkeypress ="mascara(this, '## ####-#####')" maxlength="13" name="Celular" required/>
                                            </div>
                                            <div class="col-md-1"></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-5">
                                                <label for="exampleInputPergunta">Pergunta de segurança</label>
                                                <input type="text" class="form-control" id="" name="PerguntaS" required/>
                                            </div>
                                            <div class="col-md-5">
                                                <label for="exampleInputResposta">Resposta de segurança</label>
                                                <input type="text" class="form-control" id="" name="Resposta" required/>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-1"></div> 
                                            <div class="col-md-3">
                                                <label for="exampleInputEstado">Estado</label>
                                                <br>
                                                <div class="option">
                                                    <?php
                                                    require('listaUFs.php');
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="exampleInputCidade">Cidade</label>
                                                <br>
                                                <div  id="divCidade" class="option">
                                                    <?php
                                                    require('listaCidades.php');
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="exampleInputLocalidade">Localidade</label>
                                                <br>
                                                <div  id="divLocalidade" class="option" name="localidade">
                                                    <?php
                                                    require('listalocalidade.php');
                                                    ?>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-1"></div> 
                                            <div class="col-md-10">	 
                                                <label for="exampleInputFile">
                                                    Foto de perfil
                                                </label>
                                                <input type="file" id="" name="midia" />      
                                            </div>
                                        </div>
                                        <div class="col-md-1"></div>
                                    </div>

                                    <p class="help-block">                                        
                                    </p>                
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-cadastro" ><b>
                                                    Cadastrar
                                                    <span class="glyphicon glyphicon-check" aria-hidden="true"></span></b>
                                            </button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>


                    
                </div>
                 <?php
                    require('footer.php');
                    ?>
               
                </body>
                </html>
