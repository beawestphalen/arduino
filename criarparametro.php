		<html><head>
		        <meta charset="utf-8">
		        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		        <meta name="viewport" content="width=device-width, initial-scale=1">
		        <title>Arduíno</title>
		        <link href="css/bootstrap.min.css" rel="stylesheet">
		        <link href="css/criarparametro.css" rel="stylesheet">
		    </head>
            <body>
            <?php 
            require('menu.php');
            ?>   
		            <div class="container-fluid">
		                <div class="row">
		                    <div class="topo"></div>
		                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
		                        <div class="btn-group" role="group">
		                            <a href="sensores.php" class="btn btn-default">Sensores</a>
		                        </div>
		                        <div class="btn-group" role="group">
		                            <a href="parametros.php" class="btn btn-default btn-informacoes">Parâmetros</a>
		                        </div>
		                    </div>
		                </div>
		               
		                   
		                   
		                    <div class="container">
		                        <div class="row">
		                            <div class="col-md-12">
		                                <div class="jumbotron"> 
		                                    <h1 class="text-center">Criar parâmetro</h1> 
                                            <div class="row">
                                                <div class="col-md-12"></div>                                            
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12"></div>                                            
                                            </div>
		                                    <form action="processarCriarParametros.php" method="POST" role="form">
		                         <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-2">
											<label for="exampleInputText">Declividade</label>
                                            </br>
                                            <div class="option">
                                        <select name="declividade" type="text" onchange="">
											<option> </option>
                                            <option value='1'>40°-60°</option>
                                            <option value='2'>60°-90°</option>                                   
                                        </select>                                      
												</div>
                                            </div>
                                			<div class="col-md-3">                                   
                                    <div class="form-group">      
                                    <label for="exampleInputText">Intensidade da chuva</label>
                                         </br>
												<div class="option">
                                        <select name="intensidade" type="text" onchange="">
											<option> </option>
                                            <option value='1'>Chuva fraca</option>
                                            <option value='3'>Chuva moderada</option>
                                            <option value='4'>Chuva forte</option>
                                            <option value='5'>Chuva muito forte</option>                                      
                                        </select>                                      
												</div>
											</div>
										</div>
                                    
                            <div class="col-md-2">
                                 <div class="solo">
										<div class="form-group">
										<label for="exampleInputText">Tipo do solo</label>
                                         <br>
                                            <div class="option">
                                        <select name="solo" type="text" onchange="">
											<option> </option>
                                            <option value='1'>Argiloso</option>
                                            <option value='2'>Arenoso</option>                          
                                        </select>                                      
											</div>
												</div>
											</div>   
										</div>
                                        <div class="col-md-2"></div>
                                    </div>
                               
                                
								 <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-cadastro" ><b>
                                            Criar
                                            <span class="glyphicon glyphicon-saved" aria-hidden="true"></span></b>
                                        </button>                                    
                                    </div>
								</div>             
						     </div>
						</form>  
                    </div>
                </div>
            </div>
        </div>
    </div>

    
		                <script src="js/jquery.min.js"></script>
		                <script src="js/bootstrap.min.js"></script>
		                <script src="js/scripts.js"></script></div></div>
		                <div class="section"></div>                          
		                                                        
	                                                        
		</body>
		</html>
