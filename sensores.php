
<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Arduíno</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/sensores.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>

</head>
 <body>
   <div class="container-fluid">
      <?php 
        require('menu.php');
      ?>   
      <div class="row">
           <div class="col-md-12">
			   <h1>Meus Monitoramentos</h1>
           </div>
          </div>
         <div class="row">
				<div class="col-md-12">
					<div class="row">
						<?php
              require('conexao.php');
              if(!isset($_SESSION['id'])) {
                header("location:index.php");
              }
              else{
						    $id = $_SESSION['id'];
						  }
              $contador = 0;
              $selecao = "select m.id_monitoramento, m.nomemonitoramento, m.id_localidade, m.id_tiposolo,
                          m.descricao, m.id_cidade, date_format(m.data, '%d/%m/%Y') as data,
                          m.Usuario_id_usuario, m.status, l.nome as localidade, ts.nome as tiposolo,
                          c.nome as cidade, u.login
                          from monitoramento m, localidade l, tiposolo ts, cidade c, usuario u
                          where m.id_localidade = l.id_localidade
                            and m.id_tiposolo = ts.id_tiposolo
                            and m.id_cidade = c.id_cidade
                            and m.Usuario_id_usuario = u.id_usuario
                            and m.Usuario_id_usuario ='$id' and m.status=0;";
              $resultado=mysqli_query($con,$selecao);
              $linhas =mysqli_num_rows($resultado);
              $atual = 0;
              while ($linha = mysqli_fetch_array($resultado)) {
                $nome = $linha["nomemonitoramento"];
                $texto = $linha["descricao"];
                $localidade = $linha["localidade"];
                $cidade = $linha["cidade"];
                $tiposolo = $linha["tiposolo"];
                $data = $linha["data"];
                $login = $linha["login"];
                $idmonitoramento = $linha["id_monitoramento"];
                if($contador == 1){
                  echo"<div class='row'>";
                }
                echo"
                <div class='col-md-3'>
				          <div class='thumbnail'>
					          <div class='caption'>
                      <h3>
							          $nome
						          </h3>
									    <p>Descrição: $texto</p>
									    <p>Data: $data</p>	
                      <p>Tipo de solo: $tiposolo</p>
                      <p>Cidade: $cidade</p>
                      <p>Localidade: $localidade</p>
                      <a href='processarBloquearMonitoramento.php?idmonitoramento=$idmonitoramento' class='btn btn-excluir'>
                        <b>Excluir</b>
                        <span class='glyphicon glyphicon-remove' aria-hidden='true'></span>
                      
                      </a>
                      
                      <a href='sensormonitor.php?idmonitoramento=$idmonitoramento' class='btn btn-entrar'>
										    <b>Entrar</b>
                        <span class='glyphicon glyphicon-new-window' aria-hidden='true'></span>
                      </a>
							      </div>
						      </div>
                </div>";
                if($contador == 4){
                  echo"</div>";
                  $contador = 0;
                }
                if($atual == $linhas){
                    if($contador == 1){
                      echo"<div class='col-md-9'></div></div>";
                    }
                    if($contador == 2){
                      echo"<div class='col-md-6'></div></div>";
                    }
                    if($contador == 3){
                      echo"<div class='col-md-3'></div></div>";
                    }
                }
              }
          ?>
					
					<div class="container-fluid">
					<div class="row">
					  <div class="col-md-12">
						  <a href="sensor.php"><button type="button" class="btn btn-default btn-mais">+</button></a>
					  </div>
					</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	
 </body>
</html>
                

