<html><head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Arduíno</title>
        <meta name="description" content="Source code generated using layoutit.com">
        <meta name="author" content="LayoutIt!">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/processarProblemas.css" rel="stylesheet">

      		<script src="js/jquery.min.js"></script>
        	<script src="js/bootstrap.min.js"></script>
        	<script src="js/scripts.js"></script>

    </head>
    <body>
       <?php 
	require('conexao.php');
    require('menu.php');
?>
<div class="container-fluid">
    <div class="row">
        <div class="form-group">
            <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="margintop">
                        <div class="jumbotron">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php

                                        $email = $_POST['email'];
                                        $resposta = $_POST['resposta'];

                                        $resultado = mysqli_query($con, "SELECT resposta_seguranca FROM usuario WHERE email= '$email'");
                                            while($consulta = mysqli_fetch_array($resultado)) {
                                                if($consulta['resposta_seguranca'] == $resposta){
                                                    echo("<form method='POST' action='processarEmail.php'>
                                                            <div class='row'>
                                                                <div class='col-md-4'>
                                                                    <label for='exampleInputNovaSenha'><h4>Digite a nova senha: </h4></label>
                                                                </div>
                                                                <div class='col-md-8'>
                                                                    <input type='password' class='form-control' id='' name='senha1' required/>
                                                                </div>
                                                            </div>
                                                            <div class='row'>
                                                                <div class='col-md-4'>
                                                                    <label for='exampleInputNovaSenha'><h4>Confirme a nova senha: </h4></label>
                                                                </div>
                                                                <div class='col-md-8'>
                                                                    <input type='password' class='form-control' id='' name='senha2' required/>
                                                                </div>
                                                            </div>
                                                            <div class='row'>
                                                                <div class='col-md-3'></div>
                                                                <div class='col-md-6'>
                                                                    <button type='submit' class='btn btn-noticia' value='$email'><b>
                                                                        Enviar
                                                                        <span class='glyphicon glyphicon-check' aria-hidden='true'></span></b>
                                                                    </button>
                                                                </div>
                                                                <div class='col-md-3'></div>
                                                            </div>    
                                                        </form>");
                                                    }else{
                                                            echo("<script language='javascript' type='text/javascript'>alert('Respostas diferentes!');window.location.href='index.php'</script>");
                                                    }
                                            }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>


            <div class="modal fade" id="modal-container-905037" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myModalLabel">Problemas?</h4>
                        </div>
                        <div class="modal-body">
                            <form role="form" method="POST" action="problemas.php">
                                <div class="form-group">
                                    <label for="exampleInputEmail">Insira seu e-mail para redefinir a senha:</label>
                                    <input type="email" class="form-control" id="exampleInputEmail" name="emailRecuperar">
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-noticia">
                                        <b>Enviar
                                        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                                        </b>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
