<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Arduíno</title>

        <meta name="description" content="Source code generated using layoutit.com">
        <meta name="author" content="LayoutIt!">

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/login.css" rel="stylesheet">
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/scripts.js"></script>
        <script src="js/MascaraValidacao.js"></script>
    </head>
    <body>

        <?php require('menu.php'); ?>
        <div class="container-fluid">
            <div class="form">
                <div class="row">
                    <form role="form" action="processarLogin.php" method="POST">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="jumbotron">
                                
                                <div class="row">
                                    <h1 class="cadastro"> Conecte-se </h1>
                                    <div class="borda"></div>
                                    <div class="row">
                                        <div class="col-md-12">
                                        <div class="col-md-4"></div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="inputEmail3" >
                                                        Usuário  
                                                    </label>

                                                    <input type="Usuario" name="Usuario" class="form-control" id="Usuario" />
                                                </div>
                                            </div>				
                                    </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputPassword1">Senha</label>
                                                <input type="password" class="form-control" id="Password1" name="Password1" required/>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4"></div>
                                        <div class="col-md-4">
                                            <a href="cadastro.php"><h4>Não é cadastrado?Cadastre-se!</h4></a>
                                        </div>
                                    </div>
                                    </div>
                                     
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4"></div>
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-conectar" ><b>
                                                    Conectar
                                                    <span class="glyphicon glyphicon-check" aria-hidden="true"></span></b>
                                            </button>

                                        </div>
                                    </div>
                                    </div>
                                    </form>
                                </div>


                                <div class="col-md-1"></div>


                                <p class="help-block">                                        
                                </p>


                            </div>

                        </div>
                </div>


            </div>

            <?php
            require('footer.php');
            ?>

    </body>
</html>
