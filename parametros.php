<html><head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Arduíno</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/parametros.css" rel="stylesheet">
    </head>
    <body>
            <?php 
            require('menu.php');
            ?>   
            <div class="container-fluid">
                
                
                
		                                                  
		   <div class="row" id="cabecalho">
           <div class="col-md-4">
			   <h1>Parâmetros gerais</h1>
           </div>
          </div>
         <div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6">
							<div class="thumbnail">
								<div class="caption">
									<h3>
										Chuva
									</h3>
				<table class="table">
								<thead>
									<tr>
										<th>Tipo de chuva</th>
										<th>Intensidade</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Chuva Fraca</td>
										<td>Intensidade menor que 5 mm/h.</td>
									</tr>
									<tr>
										<td>Chuva Moderada</td>
										<td>Intensidade entre 5 e 25 mm/h.</td>
									</tr>
									<tr>
										<td>Chuva Forte</td>
										<td>Intensidade ente 25,1 e 50 mm/h.</td>
									</tr>
									<tr>
										<td>Chuva Muito Forte</td>
										<td>Intensidade maior que 50 mm/h.</td>
									</tr>
								</tbody>
							</table>
							</br>
							<p>*Fonte: Alerta Rio</p></br>
							</div>
							</div>
							</div>
							<div class="col-md-6">
							<div class="thumbnail">
								<div class="caption">
									<h3>
										Umidade do solo
									</h3>
				<table class="table">
								<thead>
									<tr>
										<th>Porcentagem</th>
										<th>Identificação</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Até 1%</td>
										<td>Solo seco</td>
									</tr>
									<tr>
										<td> De 2% a 5%</td>
										<td>Solo pouco umido</td>
									</tr>
									<tr>
										<td>De 6% a 25%</td>
										<td>Solo umido</td>
									</tr>
									<tr>
										<td>De 26% a 95%</td>
										<td>Solo super umido</td>
									</tr>
									<tr>
										<td>Acima de 95%</td>
										<td>Submerso</td>
									</tr>
								</tbody>
							</table>
							<p>*Fonte: O autor</p>
							</div>
							</div>
							</div>
							</div>
						</div>
					</div>
					
					
					<div class="row">
           <div class="col-md-12">
			   <h1>Parâmetros do projeto (experimentos realizados)</h1>
           </div>
          </div>
         <div class="row">
				<div class="col-md-12">
					<div class="row">
							<div class="thumbnail">
								<div class="caption">
									<h3>
										Parâmetros gerais
									</h3>
				<table class="table">
								<thead>
									<tr>
										<th>Tipo solo</th>
										<th>Declividade</th>
										<th>Umidade inicial</th>
										<th>Umidade final</th>
										<th>Intensidade da chuva</th>
										<th>Tempo</th>
										<th>Área</th>
										<th>Deslizamento</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Arenoso</td>
										<td>90°</td>
										<td>22%</td>
										<td>93%</td>
										<td>Chuva fraca</td>
										<td>16:30</td>
										<td>*</td>
										<td>Não há possibilidade de deslizamento</td>
									</tr>
									<tr>
										<td>Arenoso</td>
										<td>90°</td>
										<td>85%</td>
										<td>106%</td>
										<td>Chuva forte</td>
										<td>1:46</td>
										<td>*</td>
										<td>Há a possibilidade de deslizamento</td>
									</tr>
									<tr>
										<td>Arenoso</td>
										<td>60°</td>
										<td>12%</td>
										<td>86%</td>
										<td>Chuva fraca</td>
										<td>17:00</td>
										<td>*</td>
										<td>Não há a possibilidade de deslizamento</td>
									</tr>
									<tr>
										<td>Arenoso</td>
										<td>60°</td>
										<td>78%</td>
										<td>106%</td>
										<td>Chuva forte</td>
										<td>1:40</td>
										<td>*</td>
										<td>Há possibilidade de deslizamento</td>
									</tr>
									<tr>
										<td>Argiloso</td>
										<td>90°</td>
										<td>63%</td>
										<td>87%</td>
										<td>Chuva fraca</td>
										<td>16:59</td>
										<td>*</td>
										<td>Não há possibilidade de deslizamento</td>
									</tr>
									<tr>
										<td>Argiloso</td>
										<td>90°</td>
										<td>63%</td>
										<td>87%</td>
										<td>Chuva forte</td>
										<td>1:20</td>
										<td>*</td>
										<td>Há possibilidade de deslizamento</td>
									</tr>
									<tr>
										<td>Argiloso</td>
										<td>60°</td>
										<td>86%</td>
										<td>95%</td>
										<td>Chuva fraca</td>
										<td>14:30</td>
										<td>*</td>
										<td>Não há possibilidade de deslizamento</td>
									</tr>
									<tr>
										<td>Argiloso</td>
										<td>60°</td>
										<td>96%</td>
										<td>96%</td>
										<td>Chuva forte</td>
										<td>1:40</td>
										<td>*</td>
										<td>Há possibilidade de deslizamento</td>
									</tr>
								</tbody>
							</table>
							</br>
							<p>*Fonte: O autor</p></br>
							</div>
							</div>
							</div>
							</div>
						</div>
				
					<div class="row">
           <div class="col-md-12">
			   <h1>Seus parâmetros</h1>
           </div>
          </div>
         <div class="row">
				<div class="col-md-12">
					<div class="row">
							<div class="thumbnail">
								<div class="caption">
									<h3>
										Parâmetros gerais
									</h3>
				<table class="table">
								<thead>
									<tr>
										<th>Tipo solo</th>
										<th>Declividade</th>
										<th>Intensidade da chuva</th>
                                    </tr>
								<!--</thead>
								<tbody>
									<tr>
										<td>Arenoso</td>
										<td>90°</td>
										<td>Chuva fraca</td>
									</tr>
								</tbody>-->
                                <?php
                                require('conexao.php');
                                session_start();
                                $resultado = mysqli_query($con,"SELECT * FROM parametrosuser WHERE Usuario_id_usuario = '".$_SESSION['id']."'");
                                while($linhas = mysqli_fetch_array($resultado)){
                                    $resultado2 = mysqli_query($con, "SELECT nome,grau,definicao FROM tiposolo, declividade, intensidade WHERE id_tiposolo='".$linhas['Tiposolo_id_tiposolo']."' AND id_declividade='".$linhas['id_declividade']."' AND id_intensidade='".$linhas['id_intensidade']."'");
                                    while($linhas2 = mysqli_fetch_array($resultado2)){
                                        echo("
                                        <tbody>
                                        <tr>
                                            <td>$linhas2[0]</td>
                                            <td>$linhas2[1]</td>
                                            <td>$linhas2[2]</td>
                                             <td> <a href='processarBloquearParametros.php?id_parametroUser=$id_parametroUser' class='btn btn-entrar'>
                        <b>Excluir</b>
                        <span class='glyphicon glyphicon-new-window' aria-hidden='true'></span>
                      </a></td>
                                        </tr>
                                        
                                        </tbody>
                                        ");
                                    }
                                }
                                ?>
							</table>
							</br>
							</br>
							</div>
							</div>
							</div>
							</div>
						</div>
					</div>
					<div class="container-fluid"></div>
					<div class="row">
					  <div class="col-md-12">
						  <a href="criarparametro.php"><button type="button" class="btn btn-default btn-mais">+</button></a>
					  </div>
					</div>
					</div>
					
                   	
                <script src="js/jquery.min.js"></script>
                <script src="js/bootstrap.min.js"></script>
                <script src="js/scripts.js"></script>              
              
</body>
</html>
