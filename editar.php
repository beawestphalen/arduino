<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Arduíno</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/editar.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="js/MascaraValidacao.js"></script>
    <script>
        function buscaCidades(str){
            var xmlhttp = new XMLHttpRequest();
             
            xmlhttp.onreadystatechange=function() {
              if (xmlhttp.readyState==4 && xmlhttp.status==200)
                document.getElementById("divCidade").innerHTML=xmlhttp.responseText;
            }
            xmlhttp.open("GET","listaCidades.php?uf=" + str,true);
            xmlhttp.send();
        }
 
        function mascara(t, mask){ 
            var i = t.value.length
            var saida = mask.substring(1,0);
            var texto = mask.substring(i) 
                if(texto.substring(0,1) != saida){
                    t.value += texto.substring(0,1);
                }
            }
            
            function buscaLocalidades(str){
            var xmlhttp = new XMLHttpRequest();
             
            xmlhttp.onreadystatechange=function() {
              if (xmlhttp.readyState==4 && xmlhttp.status==200)
                document.getElementById("divLocalidades").innerHTML=xmlhttp.responseText;
            }
            xmlhttp.open("GET","consultaLocalidades.php?cid=" + str,true);
            xmlhttp.send();
        }
    </script>
  </head>
  <body>

    <div class="container-fluid">
	<?php
		require('menu.php');
        require('conexao.php');
        $nome = "";
        $data = "";
        $sexo = "";
        $usuario = "";
        $email = "";
        $celular = "";
        $estado = "";
        $cidade = "";
        $localidade = "";
        $fotoperfil = "";
        if (isset($_GET["id"])) {
            $id = $_GET["id"];

            $consulta = "SELECT nome, data_nasc, sexo, email, telefone, login, Cidade_id_cidade, UF_id_uf, localidade_id_localidade, avatar FROM usuario WHERE id_usuario='$id';";
            $sql = mysqli_query($con,$consulta);
            while($row = mysqli_fetch_assoc($sql)){
                $nome = $row['nome'];
                $data = $row['data_nasc'];
                $sexo = $row['sexo'];
                $email = $row['email'];
                $usuario = $row['login'];
                $cidade = $row['Cidade_id_cidade'];
                $estado = $row['UF_id_uf'];
                $localidade = $row['localidade_id_localidade'];
                $fotoperfil = $row['avatar'];
                $celular = $row['telefone'];
            }
        }

    ?>
	   <div class="form">
             <div class="row">
                <form role="form" action="processarEditar.php" method="POST" enctype="multipart/form-data">
    				<div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="jumbotron">
                                <div class="row">
                                    <h1 class="cadastro" style="text-align:center;">EDITAR INFORMAÇÕES </h1>
                                     <div class="row">
                                        <div class="col-md-1"></div>   
                                        <div class="col-md-4">	 
                                            <label for="exampleInputNome">Nome completo</label>
                                            <input type="text" class="form-control" id="" name="Nome" value="<?php echo($nome);?>" required/>
                                        </div>				
                                        <div class="col-md-3">
                                            <label for="exampleInputData">Data de nascimento</label>
                                            <input type="date" class="form-control" id="" name="Data" value="<?php echo $data;?>" required/>
                                        </div>
                                        <div class="col-md-3">
                                            <label>Sexo</label>
                                                <div class="radio">
                                                    <label><input type="radio" name="Sexo" value="Feminino" value="<?php echo $sexo;?>" required/>Feminino</label>
                                                    <label><input type="radio" name="Sexo" value="Masculino" value="<?php echo $sexo;?>" required/>Masculino</label>
                                                </div>       
                                        </div>  
                                    </div>
                                    <div class="row">
                                        <div class="col-md-1"></div>
                                        <div class="col-md-3">
                                            <label for="exampleInputUsuario">Usuário</label>
                                            <input type="text" class="form-control" id="" name="Usuario" value="<?php echo $usuario;?>" required/>
                                        </div>
                                            <div class="col-md-4">
                                                <label for="exampleInputEmail1">E-mail</label>
                                                <input type="email" class="form-control" id="" name="Email" value="<?php echo $email;?>" required/>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="exampleInputTelefone">Celular</label>
                                                <input type="text" class="form-control" onkeypress ="mascara(this, '## ####-#####')" maxlength="13" name="Celular" value="<?php echo $celular;?>" required/>
                                            </div>
                                            <div class="col-md-1"></div>
                                        </div>

                                <div class="row">
									<div class="col-md-1"></div> 
                                    <div class="col-md-3">
                                        <label for="exampleInputEstado" value="<?php echo $estado;?>" >Estado</label>
                                        <br>
                                            <div class="option">
                                                <?php 
                                                require('listaUFs.php');
                                                ?>
                                            </div>
                                    </div>
                                    <div class="col-md-3">
										<div>
                                        <label for="exampleInputCidade" value="<?php echo $cidade;?>">Cidade</label>
                                         <br>
                                         <div  id="divCidade" class="option">
                                              <?php 
                                                require('listaCidades.php');
                                                ?>
										</div>
									</div>
                                    </div>
                                    <div class="col-md-3">
										<div>
                                        <label for="exampleInputLocalidade" value="<?php echo $localidade;?>">Localidade</label>
                                         <br>
                                         <div  id="divLocalidade" class="option">
                                              <?php 
                                                require('listalocalidade.php');
                                                ?>
										</div>
									</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-3">	 
                                        <label for="exampleInputFile" value="<?php echo $fotoperfil;?>" name="avatar">
                                            Foto de perfil
                                        </label>
                                        <input type="file" id="" name="midia" />      
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>
                                  
                                <p class="help-block">                                        
                                        </p>                
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-cadastro" ><b>
                                            Editar
                                            <span class="glyphicon glyphicon-check" aria-hidden="true"></span></b>
                                        </button>
                                    
                                    </div>
                                </div>
                        </div>
					</form>
				</div>
			
        </div>

</div>

  </body>
</html>
