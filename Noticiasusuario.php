<html><head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/administrador.css" rel="stylesheet">
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/scripts.js"></script>
        <script src="js/MascaraValidacao.js"></script>
        <title>Arduíno</title>
    </head>
    <body>
        <div class="container-fluid">
            <?php
            require('menu.php');
            ?>
        </div>
        <div class="topo"></div>
        <div class="container-fluid">
            <div class="col-md-12">
                <div class="noticias">
                    <center><h2>Notícias</h2></center>
                </div>
            </div>   
        </div>    
        <?php
        $contador = 0;
        require_once("conexao.php");
        $sql = "select *, date_format(data, '%d/%m/%Y') as dataformatada from noticias order by id_noticia desc;";
        $resultado = mysqli_query($con, $sql);
        $linhas = mysqli_num_rows($resultado);
        $atual = 0;
        while ($linha = mysqli_fetch_array($resultado)) {
            $contador = $contador + 1;
            $atual = $atual + 1;
            $id = $linha["id_noticia"];
            $titulo = $linha["titulo"];
            $texto = $linha["texto"];
            $data = $linha["dataformatada"];
            $imagem_noticia = $linha["imagem_noticia"];
            $link = $linha["link_noticia"];
            if ($contador == 1) {
                echo "<div class='row'>";
            }
            echo "<div class='col-md-4'>
                            <div class='thumbnail'>
                                <img src='" . $imagem_noticia . "'/>
                                <div class='caption'>
                                    <h3>" . $titulo . "</h3> 
                                    <p>" . $texto . "</p>
                                    <p><a class='btn btn-noticia' href='" . $link . "' target='_blank'><b>Saiba mais <span class='glyphicon glyphicon-share-alt' aria-hidden='true'></span></b></a>
</p>
</div>
                            </div>
                          </div>";

            if ($contador == 3) {
                echo "</div>";
                $contador = 0;
            }
            if ($atual == $linhas) {
                if ($contador == 1) {
                    echo "<div class='col-md-8'></div></div>";
                }
                if ($contador == 2) {
                    echo "<div class='col-md-4'></div></div>";
                }
            }
        }
        ?>
    </div>               
    <?php require 'footer.php' ?>

</body>
</html>
