<html><head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/administrador.css" rel="stylesheet">
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/scripts.js"></script>
        <script src="js/MascaraValidacao.js"></script>
        <title>Arduíno</title>
    </head>
    <body>
        <div class="container-fluid">
            <?php
            require('menu.php');
            ?>
        </div>
        <div class="topo"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="topo"></div>
                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <a href="cadastroNoticia.php" class="btn btn-default">Cadastro de Notícias</a>
                    </div>                       
                    <div class="btn-group" role="group">
                        <a href="listagemUsuarios.php" class="btn btn-default">Lista de Usuários</a>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="perguntasUsuarios.php" class="btn btn-default">Dúvidas dos Usuários</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="jumbotron" style="margin-bottom: 8%;background-color: #101010;color: rgb(0, 151, 156);margin-top: 10%;">                                
                            <div class="row">
                            <h1 class="text-center">BEM-VINDO ADMINISTRADOR!</h1> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>               
        <?php require 'footer.php' ?>
    </body>
</html>
