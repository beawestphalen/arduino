
<html>
    <head>
        <link href="css/menu.css" rel="stylesheet">

    </head>	

    <?php
    session_start();
    if (!isset($_SESSION['usuario'])) {
        echo('
            <div class="row">
		    <div class="col-md-12">
			    <nav class="navbar navbar-default navbar-fixed-top" id="bla"role="navigation">
				    <div class="navbar-header marca" >
					     
					       
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
							</button> 
				    </div>
				    <div class="collapse navbar-collapse" id="bla">
					  
					    <ul class="nav navbar-nav navbar-right">
                                            <li>
						<a class="color_menu" href="login.php">Conecte-se</a>	
						</li>		
					</div>
				    </ul>
				</li>
			    </ul>
			</div>
		    </nav>
		</div>
	   </div>
	    ');
    } else if ($_SESSION['usuario'] == 'admin') {
        echo('
			    <div class="container-fluid">
		<div class="row">
		    <div class="col-md-12">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			    <div class="navbar-header marca">
				<a class="navbar-brand" href="index.php"><img class="brand" alt="Marca" src="brand.png"></a>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				    <span class="sr-only">Toggle navigation</span>
				    <span class="icon-bar"></span>
				    <span class="icon-bar"></span>
				    <span class="icon-bar"></span>
				</button>
			    </div>
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
				    <li>
					<a href="index.php#sobre">Sobre</a>
				    </li>
				    <li>
					<a href="Noticias.php">Notícias</a>
				    </li>
                    <li>
					<a href="administrador.php">Administração</a>
				    </li>                   
				</ul>
				<ul class="nav navbar-nav navbar-right">
                		<li>		    
                            <a href="sair.php">Sair <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span></a>
					    </li>
					</ul>
				    </li>
				</ul>
			    </div>
			</nav>
		    </div>
		</div>
	    </div>
		    ');
    } else {
        echo('
            <div class="row">
		    <div class="col-md-12">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
			    <div class="navbar-header marca">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				    <span class="sr-only">Toggle navigation</span>
				    <span class="icon-bar"></span>
				    <span class="icon-bar"></span>
				    <span class="icon-bar"></span>
				</button>
			    </div>
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
				    <li>
					<a href="principal.php">Principal</a>
				    </li>
                                    <li>
					<a href="sensores.php">Sensores</a>
				    </li>
                                    <li>
					<a href="parametros.php">Parametros</a>
				    </li>
				    <li>
					<a href="Noticiasusuario.php">Notícias</a>
				    </li>
				    <li>
					<a href="duvidas.php">Dúvidas</a>
				    </li>
                    <li>
					<a href="respostas.php">Respostas</a>
				    </li>
                     
				</ul>
				<ul class="nav navbar-nav navbar-right">
				    <li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Perfil <strong class="caret"></strong></a>
					<ul class="dropdown-menu">
					    <li>
						<a href="perfil.php"><b>Editar </b><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
					    </li>
					    <li>
						<a href="sair.php"><b>Sair </b><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span></a>
					    </li>
					</ul>
				    </li>
				</ul>
			    </div>
			</nav>
		    </div>
		</div>
		    ');
    }
    ?>
</html>
