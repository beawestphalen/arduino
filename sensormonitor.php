
<html><head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Arduíno</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/sensormonitor.css" rel="stylesheet">
        
        <script>
         function Ajax(){
        var xmlHttp;
        try{    
                xmlHttp=new XMLHttpRequest();// Firefox, Opera 8.0+, Safari
        }
        catch (e){
                try{
                        xmlHttp=new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
                }
                catch (e){
                    try{
                                xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        catch (e){
                                alert("No AJAX!?");
                                return false;
                        }
                }
        }

        xmlHttp.onreadystatechange=function(){
                if(xmlHttp.readyState==4){
                        document.getElementById('ReloadThis').innerHTML=xmlHttp.responseText;
                            setTimeout('Ajax()',120000);
                                        }
                                            }
        xmlHttp.open("GET","sensorrefrash.php",true); // aqui configuramos o arquivo
        xmlHttp.send(null);
            }

        window.onload=function(){
            setTimeout('Ajax()',120000); // aqui o tempo entre uma atualização e outra
        }
        </script>
        <?php
			session_start();
		?>
    </head>
    <body>
            
        <div class="container-fluid">
            <?php 
            require('menu.php');
            ?>   
            
             <div class="row">
                    </br> 
                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                        <div class="btn-group" role="group">
                            <a href="sensores.php" class="btn btn-default">Sensores</a>
                        </div>
                        <div class="btn-group" role="group">
                            <a href="relatorios.php" class="btn btn-default">Relatórios</a>
                        </div>
                        <div class="btn-group" role="group">
                            <a href="parametros.php" class="btn btn-default">Parâmetros</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

			<div class="section">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
                       <?php
                        require('conexao.php');
                        if(!isset($_SESSION['id'])){
                        header("location:index.php");
                        }else{
						$id = $_SESSION['id'];
						}
                        $idmonitoramento = $_GET["idmonitoramento"];
                        $selecao = "select * from monitoramento where idmonitoramento='$idmonitoramento';";
                        $selecao2=mysqli_query($con,$selecao);
                        while ($linha = mysqli_fetch_array($selecao2)) {
                            $nome= $linha["nomemonitoramento"];
                                $texto= $linha["descricao"];
                                $localidade= $linha["id_localidade"];
                                    $cidade= $linha["id_cidade"];
                                        $tipo= $linha["id_tiposolo"];
                                         $data= $linha["data"];
                                        
                        $localidade = "select * from localidade where id_localidade='$localidade';";
                        $localidade2=mysqli_query($con,$localidade);
                        while ($linha = mysqli_fetch_array($localidade2)) {
                        $nomelocal=$linha["nome"];
                        }
                        
                         $selectcidade = "select * from cidade   where id_cidade='$cidade';";
                        $selectcidade2=mysqli_query($con,$selectcidade);
                        while ($linha = mysqli_fetch_array($selectcidade2)) {
                        $nomecidade=$linha["nome"];
                        }
                        
                        $selectsolo = "select * from tiposolo   where id_tiposolo='$tipo';";
                        $selectsolo2=mysqli_query($con,$selectsolo);
                        while ($linha = mysqli_fetch_array($selectsolo2)) {
                        $nomesolo=$linha["nome"];
                        }
                        echo"
							<div>
									<h3>
										$nome
									</h3>
									<p>
									Descrição: $texto
									<p>Data: $data</p>	
                                    <p>Tipo de solo: $nomesolo</p>
                                      <p>Cidade: $nomecidade
                                        </p>
                                        <p>Localidade: $nomelocal</p>
									</p>
									
										<button type='submit' class='btn btn-entrar' ><b>
                                            Entrar
                                            <span class='glyphicon glyphicon-new-window' aria-hidden='true'></span></b>
										<button type='submit' class='btn btn-excluir' ><b>
                                            Excluir
                                            <span class='glyphicon glyphicon-remove' aria-hidden='true'></span></b>
									
                            
							</div>
						</div>";
                            }
                        
                        ?>
                              <?php
                                    require("includes/comunicacao_serial.php");
                                    CheckDadosCom();
                                ?>

                            
                            <!--<form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
                                    <center><b>Data:  </b>
                                    <input type="date" name="calendario" id="calendario" min="2013-01-01" title="Informe uma data para busca. Ex: 2013-12-12" />
                                    <input class= "cor" type="submit" name="pesquisar" id="pesquisar" value="Buscar" />
                                    </center>
                            </form>
                                    <br>
                                    <div id="ReloadThis">                        <center>-->
                            <?php
                                $idmonitoramento = $_GET["idmonitoramento"];
                                echo "<form method='POST' action='sensormonitor.php?idmonitoramento=$idmonitoramento'>
                                        <center><b>Data:</b>";
                                        if (isset($_POST['calendario'])) {
                                            $data = date("Y-m-d");
                                            $calendario = $_POST['calendario'];
                                            echo "<input type='date' id='calendario' name='calendario' value='$calendario' />";
                                        }
                                        else {
                                            $calendario = date("Y-m-d");
                                            echo "<input type='date' id='calendario' name='calendario' value='$calendario' />";
                                        }
                                        echo "
                                            <input class='cor' type='submit' name='pesquisar' id='pesquisar' value='Buscar' />
                                        </center>
                                    </form>
                                    <div id='ReloadThis'> <center>
                                     ";
                            ?>
        <?php
            if(isset($_POST['pesquisar']))
            {
                    $data = $_POST['calendario'];
                        include("includes/func_sql.php");


                            $retorno = ConsultaTemp($data);

		echo "<table border=\"0\">";
		echo "<thead><tr><th width=\"120\"><b>Data</b></th><th width=\"120\"><b>Hora</b></th><th width=\"120\"><b>Umidade</b></th><th width=\"120\"><b>Sensor</b></th><th width=\"120\"><b>Nome sensor</b></th><th width=\"120\"><b>Descrição</b></th>
            <th width=\"120\"><b>Localidade</b></th><th width=\"120\"><b>Cidade</b></th><th width=\"120\"><b>Estado</b></th>
            <th width=\"120\"><b>Tipo do solo</b></th></tr></thead>";
		echo "<tbody>";
		while($consulta = mysql_fetch_row($retorno)) {
			echo "<tr><td>".$consulta[0]."</td><td>".$consulta[1]."</td><td>".$consulta[2]."</td></tr>";
		}
		echo "</tbody>";
		echo "</table>";
		
	}
	else
	{
		$con = mysqli_connect("localhost", "root", "","arduino") or print (mysqli_error());

//		mysqli_select_db("arduino", $conecta) or print(mysqli_error());


		$sql = "select umidade.id_umidade, umidade.data, umidade.hora, umidade.umidade, umidade.Sensor_id_sensor, sensor.nome as sensor, sensor.descricao, localidade.nome as localidade, cidade.nome as cidade, uf.nome as uf, tiposolo.nome as tiposolo from umidade, sensor, localidade, cidade, uf, tiposolo where umidade.Sensor_id_sensor = sensor.id_sensor and sensor.cidade_id_cidade = cidade.id_cidade and sensor.localidade_id_localidade = localidade.id_localidade and sensor.cidade_id_cidade = cidade.id_cidade and sensor.tipo_id_tiposolo = tiposolo.id_tiposolo and uf.id_uf = cidade.UF_id_uf";
		$retorno = mysqli_query($con, $sql);


		echo "<table border=\"0\">";
		echo "<thead><tr><th width=\"120\"><b>Data</b></th><th width=\"120\"><b>Hora</b></th><th width=\"120\"><b>Umidade</b></th><th width=\"120\"><b>Sensor</b></th><th width=\"120\"><b>Nome sensor</b></th><th width=\"120\"><b>Descrição</b></th>
            <th width=\"120\"><b>Localidade</b></th><th width=\"120\"><b>Cidade</b></th><th width=\"120\"><b>Estado</b></th>
            <th width=\"120\"><b>Tipo do solo</b></th></tr></thead>";
		echo "<tbody>";
		while($consulta = mysqli_fetch_array($retorno)) {
			echo "<tr><td>".$consulta["data"]."</td><td>".$consulta["hora"]."</td><td>".$consulta["umidade"]."</td><td>".$consulta["Sensor_id_sensor"]."</td><td>".$consulta["sensor"]."</td><td>".$consulta["descricao"]."</td><td>".$consulta["localidade"]."</td><td>".$consulta["cidade"]."</td><td>".$consulta["uf"]."</td><td>".$consulta["tiposolo"]."</td></tr>";
		}
		echo "</tbody>";
		echo "</table>";

		//mysql_free_result($retorno);
		mysqli_close($con);
	}
	?>
	</center>
                        </div>
						</div>
					</div>
				</div>
			</div>
		  <script src="js/jquery.min.js"></script>
                <script src="js/bootstrap.min.js"></script>
                <script src="js/scripts.js"></script>
		
</body>
</html>
