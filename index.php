<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Arduíno</title>
        <meta name="description" content="Source code generated using layoutit.com">
        <meta name="author" content="LayoutIt!">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/index.css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <?php
            require('menu.php');
            ?>
            <div class="top">
            </div>
            <div class="row">
                <div class="imagem" >  
                    <img src="img/index/1.jpg" class="img-responsive">
                </div>
            </div>

            <?php
            require('footer.php');
            ?>
            <div class="modal fade" id="modal-container-905037" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myModalLabel">Problemas?</h4>
                            <h4>Não é cadastrado? Cadastre-se!</h4>
                        </div>

                        <div class="modal-body">
                            <form role="form" method="POST" action="problemas.php">
                                <div class="form-group">
                                    <label for="exampleInputEmail">Insira seu e-mail para redefinir a senha:</label>
                                    <input type="email" class="form-control" id="exampleInputEmail" name="emailRecuperar">
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-noticia">
                                        <b>Enviar
                                            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                                        </b>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            <script src="js/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/scripts.js"></script>

        </div>
    </body>
</html>
