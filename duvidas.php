<html><head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Arduíno</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/duvidas.css" rel="stylesheet">
    </head><body>
        
            
            <?php
            session_start();
            require('menu.php');
            ?>
           <div class="container-fluid"> 
            <div class="form">
                <div class="row">
                    <form method="post" action="processarDuvida.php" role="form">
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="jumbotron">
                                    <div class="row">
                                        <h1 class="mandar text-center">Mande suas dúvidas</h1>
                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-10">
                                                <textarea name="duvida" class="form-control" id="duvida" rows='15'></textarea>
                                            </div>
                                            <div class="col-md-1"></div>
                                        </div>
                                        <p class="help-block"></p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-entrar">
                                                    <b>Enviar
                                                        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                                                    </b>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    
                    </div></form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12"></div>
            </div>
        </div>
        
        
        <div class="row">
            <div class="col-md-12"></div>
        </div>
		
		<?php require('footer.php'); ?>
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/scripts.js"></script>
    

</body></html>
