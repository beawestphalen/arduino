<?php

    
	include 'conexao.php';
	
	$consulta = 'select id_intensidade, definicao from intensidade';
	
	$resultado = $con->query($consulta);
	
    if(!$resultado) {
		printf("Erro na consulta: %s\n", $con->error);
		die();
	}
    echo "<select name='nome' onchange='buscaIntensidade(this.value)'>";
	echo "<option>----Selecione um valor para intensidade----</option>";
	while($linha = $resultado->fetch_array(MYSQLI_ASSOC)) {
    
		echo "<option value='".$linha['id_intensidade']."'>".$linha['definicao']."</option>";
		
	}
	echo "</select>";

	include 'desconecta.php';
	
?>
