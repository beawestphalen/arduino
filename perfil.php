<?php
session_start();
?>
<html><head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Arduíno</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/perfil.css" rel="stylesheet">
    </head>
    <body>        
        <?php
        require('menu.php');
        ?>            
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/scripts.js"></script>

        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        </div>
        <?php
        require('conexao.php');
        if (!isset($_SESSION['id'])) {
            header("location:index.php");
        } else {
            $id = $_SESSION['id'];
            $sintaxesql = "select nome, login, Cidade_id_cidade, UF_id_uf, localidade_id_localidade, telefone, email, sexo, date_format(data_nasc, '%d/%m/%Y') as dataformatada, avatar from usuario where id_usuario = '$id';";
            $resultado = mysqli_query($con, $sintaxesql);

            while ($linha = mysqli_fetch_array($resultado)) {
                $nome = $linha["nome"];
                $login = $linha["login"];
                $email = $linha["email"];
                $data_nasc = $linha["dataformatada"];
                $sexo = $linha["sexo"];
                $telefone = $linha["telefone"];
                $cidade = $linha["Cidade_id_cidade"];
                $uf = $linha["UF_id_uf"];
                $localidade = $linha["localidade_id_localidade"];

                $idfotoperfil = $linha["avatar"];

                $localidade = "select * from localidade where id_localidade='$localidade';";
                $localidade2 = mysqli_query($con, $localidade);
                while ($linha = mysqli_fetch_array($localidade2)) {
                    $nomelocal = $linha["nome"];
                }

                $selectcidade = "select * from cidade   where id_cidade='$cidade';";
                $selectcidade2 = mysqli_query($con, $selectcidade);
                while ($linha = mysqli_fetch_array($selectcidade2)) {
                    $nomecidade = $linha["nome"];
                }

                $selectuf = "select * from cidade, uf   where cidade.UF_id_uf=uf.id_uf and UF_id_uf='$uf' ;";
                $selectuf2 = mysqli_query($con, $selectuf);
                while ($linha = mysqli_fetch_array($selectuf2)) {
                    $nomeuf = $linha["nome"];
                }
            }
        }
        $id = $_SESSION["id"];
        echo " 
            </br></br></br></br></br></br></br></br>
            <div class='section'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-md-5'>
                            <img src='$idfotoperfil' class='center-block img-responsive'/>                           
                        </div>
                        <div class='col-md-6'>
                            <h1>
                                <b>$nome</b>
                            </h1>
                            <h2>$login</h2>
                            <p>
							    <h4> <span class='glyphicon glyphicon-user' aria-hidden='true'> $sexo </br>
							       <br> <span class='glyphicon glyphicon-calendar' aria-hidden='true'> $data_nasc </br>
                                    <br> <span class='glyphicon glyphicon-envelope' aria-hidden='true'> $email</br>
                                    <br> <span class='glyphicon glyphicon-phone' aria-hidden='true'> $telefone</br>
                                    <br><span class='glyphicon glyphicon-globe' aria-hidden='true'> $nomecidade - $nomeuf</br>
                                    </br><span class='glyphicon glyphicon-flag' aria-hidden='true'> Região: $nomelocal</br>
                                </h4>   
                            </p>
                             <a href='editar.php?id=$id' class='btn btn-editar'>
                                <b>Editar</b>
                                <span class='glyphicon glyphicon-pencil' style='text-center' aria-hidden='true'></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
           ";
        ?>
         <?php
        require('footer.php');
        ?> 

    </body>
</html>

